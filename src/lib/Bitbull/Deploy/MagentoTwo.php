<?php
namespace Bitbull\Deploy;

use Composer\Script\Event;
use Dotenv\Dotenv;
use Dotenv\Exception\InvalidPathException;

define('DS', DIRECTORY_SEPARATOR);

class MagentoTwo
{
    private static $_vars = array(
        "APP_KEY",
        "DB_HOSTNAME",
        "DB_USERNAME",
        "DB_PASSWORD",
        "DB_NAME",
        "REDIS_ENDPOINT",
        "SESSION_SAVE",
        "PSR0_NAMESPACES",
        "ADMIN_FRONTNAME",
        "ERROR_EMAIL",
        "MAGE_MODE"
    );

    private static $_config = array();

    private static $_optionals = array(
        "PSR0_NAMESPACES",
        "REDIS_ENDPOINT",
        "ERROR_EMAIL",
        "LOCALES"
    );

    public static function generateEnvConfig(Event $event)
    {
        $event->getIO()->write("Generate env.php configuration file");

        $magentoRootDir = self::getMagentoRootDir($event);
        self::loadEnvVars($magentoRootDir);

        if (self::isConfigurationFileFound($magentoRootDir) && !self::isConfigurationFileOverwritable()) {
            $event->getIO()->write("The configuration file already exists and will not be updated.");
            return;
        }

        foreach (self::$_vars as $var) {
            if (!array_key_exists($var, $_SERVER) && !in_array($var, self::$_optionals)) {
                throw new \RuntimeException("Missing environment variable \"{$var}\"");
            }

            if (array_key_exists($var, $_SERVER)) {
                $event->getIO()->write("Env variable key -> \"$var\": \"$_SERVER[$var]\"");
                self::$_config[$var] = $_SERVER[$var];
            }
        }

        $templateFilename = 'env.php.twig';
        $configurationContent = Utils::getConfigurationContent($event->getComposer(), $templateFilename, self::$_config);
        file_put_contents(self::getConfigurationFilePath($magentoRootDir), $configurationContent);
    }

    public static function enableMaintenanceMode(Event $event)
    {
        self::enableMagentoCli($event);

        if (self::isDeveloperMode($event)) {
            $event->getIO()->write("Enable maintenance mode: skipped in developer mode");
            return;
        }
        $event->getIO()->write("Enable maintenance mode");
        $magentoRootDir = self::getMagentoRootDir($event);
        $command = self::getMagentoCli($magentoRootDir) . ' maintenance:enable';
        exec($command);
    }

    public static function disableMaintenanceMode(Event $event)
    {
        if (self::isDeveloperMode($event)) {
            $event->getIO()->write("Disable maintenance mode: skipped in developer mode");
            return;
        }
        $event->getIO()->write("Disable maintenance mode");
        $magentoRootDir = self::getMagentoRootDir($event);
        $command = self::getMagentoCli($magentoRootDir) . ' maintenance:disable';
        exec($command);
    }

    public static function flushCache(Event $event)
    {
        $event->getIO()->write("Flush cache");
        $magentoRootDir = self::getMagentoRootDir($event);
        $command = self::getMagentoCli($magentoRootDir) . ' cache:flush';
        exec($command, $output);
        $event->getIO()->write(implode(PHP_EOL, $output).PHP_EOL);
    }

    public static function setCacheMode(Event $event)
    {
        self::flushCache($event);

        $magentoRootDir = self::getMagentoRootDir($event);
        self::loadEnvVars($magentoRootDir);
        $mode = strtolower(trim($_SERVER['CACHE_MODE']));

        // if given $mode doesn't match allowed one, enable cache to avoid unintentional performance downgrade
        if (!in_array($mode, ['enable', 'disable'])) {
            $mode = 'enable';
        }

        $event->getIO()->write("Set cache mode: $mode");
        $magentoRootDir = self::getMagentoRootDir($event);
        $command = self::getMagentoCli($magentoRootDir) . ' cache:' . $mode;
        exec($command, $output);
        $event->getIO()->write(implode(PHP_EOL, $output).PHP_EOL);
    }

    /**
     * @deprecated
     */
    public static function setApplicationMode(Event $event)
    {
        $event->getIO()->write("Set application mode");
        $magentoRootDir = self::getMagentoRootDir($event);
        self::loadEnvVars($magentoRootDir);
        $mode = $_SERVER['MAGE_MODE'];
        $command = self::getMagentoCli($magentoRootDir) . ' deploy:mode:set ' . $mode;
        exec($command, $output);
        $event->getIO()->write(implode(PHP_EOL, $output).PHP_EOL);
    }

    public static function runUpgradeScripts(Event $event)
    {
        $event->getIO()->write("Run upgrade scripts");
        $magentoRootDir = self::getMagentoRootDir($event);

        # Enable all modules -> generate app/etc/config.php
        $command = self::getMagentoCli($magentoRootDir) . ' module:enable --all';
        exec($command, $output);
        $event->getIO()->write(implode(PHP_EOL, $output).PHP_EOL);

        # Run upgrade scripts
        $command = self::getMagentoCli($magentoRootDir) . ' setup:upgrade';
        exec($command, $output);
        $event->getIO()->write(implode(PHP_EOL, $output).PHP_EOL);
    }

    public static function deployStaticContent(Event $event)
    {
        if (self::isDeveloperMode($event)) {
            $event->getIO()->write("Deploy static content: skipped in developer mode");
            return;
        }

        $event->getIO()->write("Deploy static content");
        $magentoRootDir = self::getMagentoRootDir($event);
        $command = self::getMagentoCli($magentoRootDir) . ' setup:static-content:deploy ' . self::getLocales($event);
        exec($command, $output);
        $event->getIO()->write(implode(PHP_EOL, $output).PHP_EOL);
    }

    public static function compileDi(Event $event)
    {
        if (self::isDeveloperMode($event)) {
            $event->getIO()->write("Compile DI: skipped in developer mode");
            return;
        }

        $event->getIO()->write("Compile DI");
        $magentoRootDir = self::getMagentoRootDir($event);
        // Why multitenant? see http://devdocs.magento.com/guides/v2.0/config-guide/cli/config-cli-subcommands-compiler.html#config-cli-subcommands-single
        $command = self::getMagentoCli($magentoRootDir) . ' setup:di:compile-multi-tenant -q';
        exec($command, $output);
        $event->getIO()->write(implode(PHP_EOL, $output).PHP_EOL);

        // See bug [#4070](https://github.com/magento/magento2/issues/4070)
        $command = 'rm -f ' . $magentoRootDir . DS . 'var' . DS . 'di' . DS . 'relations.ser';
        exec($command);
    }

    public static function setFilePermissions(Event $event)
    {
        $event->getIO()->write("Set file permissions");
        $group = self::getWebserverUser($event);

        $magentoRootDir = self::getMagentoRootDir($event);

        $command = sprintf(
            'setfacl -R -m u:"%s":rwX -m u:`whoami`:rwX %s',
            $group,
            $magentoRootDir
        );
        exec($command);

        $command = sprintf(
            'setfacl -dR -m u:"%s":rwX -m u:`whoami`:rwX %s',
            $group,
            $magentoRootDir
        );
        exec($command);

        // Restore execution permissions on bin/magento
        self::enableMagentoCli($event);
    }

    protected static function enableMagentoCli(Event $event)
    {
        $event->getIO()->write("Set executable permission on bin/magento");
        $magentoRootDir = self::getMagentoRootDir($event);
        $command = 'chmod +x ' . self::getMagentoCli($magentoRootDir);
        exec($command);
    }

    protected static function getLocales(Event $event)
    {
        $magentoRootDir = self::getMagentoRootDir($event);
        self::loadEnvVars($magentoRootDir);

        $locales = "en_US";
        if(isset($_SERVER['LOCALES']) && trim($_SERVER['LOCALES']) != ""){
            $locales = $_SERVER['LOCALES'];
        }
        $locales = array_map('trim', explode(',', $locales));
        return '"' . implode('" "', $locales) . '"';
    }

    protected static function isConfigurationFileFound($magentoRootDir)
    {
        return (file_exists(self::getConfigurationFilePath($magentoRootDir)));
    }

    protected static function isConfigurationTemplateFileFound($magentoRootDir)
    {
        return (file_exists(self::getConfigurationTemplateFilePath($magentoRootDir)));
    }

    protected static function getConfigurationFilePath($magentoRootDir)
    {
        return $magentoRootDir . DS . 'app' . DS . 'etc' . DS . 'env.php';
    }

    protected static function getConfigurationTemplateFilePath($magentoRootDir)
    {
        return $magentoRootDir . DS . 'config' . DS . 'env.php';
    }

    protected static function isConfigurationFileOverwritable()
    {
        return (array_key_exists('OVERWRITE_CONF', $_SERVER));
    }

    protected static function getConfigurationTemplateFileContent($phpContent)
    {
        $needles = [
            '<?php',
            '<?=',
            '<?',
            '<%',
            '<SCRIPT LANGUAGE=php>',
            '<SCRIPT LANGUAGE="php">',
            '<SCRIPT LANGUAGE=\'php\'>',
        ];
        $strippedContent = str_replace($needles, '', $phpContent);
        $data = eval($strippedContent);
        return "<?php\nreturn " . var_export($data, true) . ";\n";
    }

    protected static function getMagentoRootDir(Event $event)
    {
        $vendorPath = $event->getComposer()->getConfig()->get('vendor-dir');
        $extra = $event->getComposer()->getPackage()->getExtra();
        $magentoRootDir = realpath(dirname($vendorPath) . DS . $extra['magento-root-dir']);
        return $magentoRootDir;
    }

    protected static function getMagentoCli($magentoRootDir)
    {
        return $magentoRootDir . DS . 'bin' . DS . 'magento';
    }

    /**
     * @param $projectPath
     * @param $magentoRootDir
     */
    protected static function loadEnvVars($magentoRootDir)
    {
        $projectPath = dirname($magentoRootDir);
        if (file_exists($projectPath . '/.env')) {
            // search in magento parent path first
            $dotenv = new Dotenv($projectPath);
            $dotenv->load();
        } else if (file_exists($magentoRootDir . '/.env')) {
            // fallback on magento path
            $dotenv = new Dotenv($magentoRootDir);
            $dotenv->load();
        }
    }

    protected static function isDeveloperMode(Event $event)
    {
        $magentoRootDir = self::getMagentoRootDir($event);
        self::loadEnvVars($magentoRootDir);
        $mode = $_SERVER['MAGE_MODE'];
        return (strtolower(trim($mode)) == 'developer');
    }

    protected static function getWebserverUser(Event $event)
    {
        $magentoRootDir = self::getMagentoRootDir($event);
        self::loadEnvVars($magentoRootDir);
        return strtolower(trim($_SERVER['WEBSERVER_USER']));
    }
}
