<?php
namespace Bitbull\Deploy;

use Composer\Script\Event;
use Composer\Composer;
use DOMDocument;
use DOMXPath;

class Utils
{
    /**
     * Retrieve path to template for configuration file
     *
     * @param Composer $composer
     * @param string $fileName
     * @return string
     */
    private static function getConfigurationTplFilePath(Composer $composer, $fileName)
    {
        $repositoryManager = $composer->getRepositoryManager();
        $installationManager = $composer->getInstallationManager();
        $localRepository = $repositoryManager->getLocalRepository();

        $installPath = '';

        $packages = $localRepository->getPackages();
        foreach ($packages as $package) {
            if ($package->getName() === 'bitbull/magento-deploy') {
                $installPath = $installationManager->getInstallPath($package);
                break;
            }
        }

        return $installPath . '/src/templates/' . $fileName;
    }

    /**
     * Retrieve and parse configuration template
     *
     * @param Composer $composer
     * @param string $fileName
     * @param array $config
     * @return string
     */
    public static function getConfigurationContent(Composer $composer, $fileName, $config)
    {
        $template = file_get_contents(self::getConfigurationTplFilePath($composer, $fileName));

        $twig = new \Twig_Environment(new \Twig_Loader_String());

        return $twig->render(
            $template,
            $config
        );
    }

    public static function removeDirRecursive($dirPath){

        if (file_exists($dirPath) && is_dir($dirPath))
        {
            $recursiveDirectoryIterator = new \RecursiveDirectoryIterator($dirPath, \FilesystemIterator::SKIP_DOTS);
            foreach (new \RecursiveIteratorIterator($recursiveDirectoryIterator, \RecursiveIteratorIterator::CHILD_FIRST) as $path)
            {
                $path->isDir() ? rmdir($path->getPathname()) : unlink($path->getPathname());
            }
            rmdir($dirPath);
        }
    }

    /**
     * Replaces the first occurrence of the specified XPath node with a new text value
     *
     * @param string $xmlFilePath
     * @param string $xPathNodeParent
     * @param string $xPathNode
     * @param string $newValue
     */
    public static function replaceXmlTextNode($xmlFilePath, $xPathNodeParent, $xPathNode, $newValue)
    {
        $xml = New DOMDocument();
        $xml->load($xmlFilePath);

        $xpath = new DOMXpath($xml);
        $parentNode = $xpath->query($xPathNodeParent)->item(0);

        $newNode = $xml->createElement($xPathNode);
        $newNode->appendChild($xml->createTextNode((string)$newValue));

        $parentNode->replaceChild($newNode, $xpath->query($xPathNodeParent . '/' . $xPathNode)->item(0));

        // write new config
        file_put_contents($xmlFilePath, $xml->saveXML());
    }
}
