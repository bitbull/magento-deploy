<?php
namespace Bitbull\Deploy;

use Composer\Script\Event;

class Magento
{
    //Overwrite conf flag
    const OVERWRITE_CONF = "OVERWRITE_CONF";

    const COMPOSER_VENDOR_PATH = 'COMPOSER_VENDOR_PATH';

    private static $_vars = array(
        "APP_KEY",
        "DB_HOSTNAME",
        "DB_USERNAME",
        "DB_PASSWORD",
        "DB_NAME",
        "REDIS_ENDPOINT",
        "SESSION_SAVE",
        "PSR0_NAMESPACES",
        "ADMIN_FRONTNAME",
        "ENABLE_REDIS_SESSION",
        "ERROR_EMAIL",
        "ENABLE_FPC",
    );

    private static $_config = array();

    private static $_optionals = array(
        "PSR0_NAMESPACES",
        "REDIS_ENDPOINT",
        "ENABLE_REDIS_SESSION",
        "ERROR_EMAIL",
        "ENABLE_FPC",
    );

    public static function generateLocalXml(Event $event)
    {
        $vendorPath = $event->getComposer()->getConfig()->get('vendor-dir');
        $extra = $event->getComposer()->getPackage()->getExtra();

        $magentoRootDir = realpath(dirname($vendorPath) . '/' . $extra['magento-root-dir']);
        $projectPath = dirname($magentoRootDir);

        $event->getIO()->write("<info>Generate local XML configuration for Magento</info>");

        if (self::existsLocalConf($magentoRootDir) && !self::overwriteConf()) {
            $event->getIO()->write("Local conf already exists, i will skip it!");
            return;
        }

        if (file_exists($projectPath . '/.env')) {
            $dotenv = new \Dotenv\Dotenv($projectPath);
            $dotenv->load();
        } else if (file_exists($magentoRootDir . '/.env')) {
            $dotenv = new \Dotenv\Dotenv($magentoRootDir);
            $dotenv->load();
        }

        // overwrites default {{root_dir}}/vendor whenever is forced by env var or magento's root is different form project root
        if (array_key_exists(self::COMPOSER_VENDOR_PATH, $_SERVER)) {
            self::$_config[self::COMPOSER_VENDOR_PATH] = $_SERVER[self::COMPOSER_VENDOR_PATH];
        } else if ($magentoRootDir != dirname($vendorPath)) {
            self::$_config[self::COMPOSER_VENDOR_PATH] = $vendorPath;
        } else {
            self::$_config[self::COMPOSER_VENDOR_PATH] = '';
        }

        foreach (self::$_vars as $var) {
            if (!array_key_exists($var, $_SERVER) && !in_array($var, self::$_optionals)) {
                throw new \RuntimeException("Missing environment variable \"{$var}\"");
            }

            if (array_key_exists($var, $_SERVER)) {
                $event->getIO()->write("Env variable key -> \"$var\": \"$_SERVER[$var]\"");

                switch ($var) {
                    case "PSR0_NAMESPACES":
                        $_SERVER[$var] = explode(',', $_SERVER[$var]);

                        break;
                    case "ENABLE_REDIS_SESSION":
                        if (!($_SERVER["REDIS_ENDPOINT"] === '' ) && $_SERVER["ENABLE_REDIS_SESSION"] == true) {
                            self::enableRedisSessionModule($event);
                        }
                        break;
                    case "ERROR_EMAIL":
                        if (!($_SERVER["ERROR_EMAIL"] === '' )) {
                            self::setErrorMailAddress($event, $_SERVER["ERROR_EMAIL"]);
                        }
                        break;
                }

                self::$_config[$var] = $_SERVER[$var];
            }
        }

        $templateFilename = 'local.xml.twig';
        $configurationContent = Utils::getConfigurationContent($event->getComposer(), $templateFilename, self::$_config);
        $configurationFilepath = self::getConfigurationFilePath($magentoRootDir);
        file_put_contents($configurationFilepath, $configurationContent);

        $event->getIO()->write("Saved local.xml configuration");
    }

    /**
     * Set maintenance mode only if there is an upgrade script to run
     */
    public static function setMaintenanceMode(Event $event)
    {
        $vendorPath = $event->getComposer()->getConfig()->get('vendor-dir');
        $binPath = $vendorPath . DIRECTORY_SEPARATOR . 'bin' . DIRECTORY_SEPARATOR;
        exec($binPath . 'n98-magerun --skip-root-check sys:setup:compare-versions --format json', $output);
        $modules = json_decode(implode('', $output), true);
        foreach ($modules as $module) {
            if ($module['Status'] != 'OK') {
                $extra = $event->getComposer()->getPackage()->getExtra();
                $magentoRootPath = realpath(dirname($vendorPath) . DIRECTORY_SEPARATOR . $extra['magento-root-dir']);
                exec('touch ' . $magentoRootPath . DIRECTORY_SEPARATOR . 'maintenance.flag');
                break;
            }
        }
    }

    /**
     * Remove unwanted dirs (like Magento Connect's) and files
     */
    public static function cleanupWebFolder(Event $event)
    {
        $unwantedDirs = array(
            'downloader',
            'dev',
        );

        $unwantedFiles = array(
            'LICENSE.html',
            'LICENSE.txt',
            'LICENSE_EE.html',
            'LICENSE_EE.txt',
            'LICENSE_AFL.txt',
            'RELEASE_NOTES.txt',
            '.htaccess.sample',
            'install.php',
            'index.php.sample',
            'php.ini.sample',
            'Readme.md',
        );

        $vendorPath = $event->getComposer()->getConfig()->get('vendor-dir');
        $extra = $event->getComposer()->getPackage()->getExtra();
        $magentoRootPath = realpath(dirname($vendorPath) . DIRECTORY_SEPARATOR . $extra['magento-root-dir']);

        foreach ($unwantedDirs as $dir){
            $dirPath = $magentoRootPath . DIRECTORY_SEPARATOR . $dir;
            if (is_dir($dirPath)) {Utils::removeDirRecursive($dirPath);}
        }
        foreach ($unwantedFiles as $file){
            $filePath = $magentoRootPath . DIRECTORY_SEPARATOR . $file;
            if (file_exists($filePath)) {unlink($filePath);}
        }
    }

    public static function enableRedisSessionModule(Event $event)
    {
        $vendorPath = $event->getComposer()->getConfig()->get('vendor-dir');
        $extra = $event->getComposer()->getPackage()->getExtra();
        $magentoRootPath = realpath(dirname($vendorPath) . DIRECTORY_SEPARATOR . $extra['magento-root-dir']);

        Utils::replaceXmlTextNode(
            $magentoRootPath . '/app/etc/modules/Cm_RedisSession.xml',
            '/config/modules/Cm_RedisSession',
            'active',
            'true'
        );

        $event->getIO()->write("Module Cm_RedisSession.xml enabled");
    }

    /**
     * @param Event $event
     * @param string $email <p>
     * recipient format follows PHP's mail() specification (RFC 2822)
     * </p>
     */
    public static function setErrorMailAddress(Event $event, $email)
    {
        $vendorPath = $event->getComposer()->getConfig()->get('vendor-dir');
        $extra = $event->getComposer()->getPackage()->getExtra();
        $magentoRootPath = realpath(dirname($vendorPath) . DIRECTORY_SEPARATOR . $extra['magento-root-dir']);

        Utils::replaceXmlTextNode(
            $magentoRootPath . '/errors/local.xml',
            '/config/report',
            'email_address',
            $email
        );

        $event->getIO()->write("Wrote new email address(es) in errors/local.xml");
    }

    public static function unsetMaintenanceMode(Event $event)
    {
        $vendorPath = $event->getComposer()->getConfig()->get('vendor-dir');
        $extra = $event->getComposer()->getPackage()->getExtra();
        $magentoRootPath = realpath(dirname($vendorPath) . DIRECTORY_SEPARATOR . $extra['magento-root-dir']);
        exec('rm ' . $magentoRootPath . DIRECTORY_SEPARATOR . 'maintenance.flag');
    }

    public static function flushCache(Event $event)
    {
        $vendorPath = $event->getComposer()->getConfig()->get('vendor-dir');
        $binPath = $vendorPath . DIRECTORY_SEPARATOR . 'bin' . DIRECTORY_SEPARATOR;
        exec($binPath . 'n98-magerun --skip-root-check cache:flush', $output);
    }

    public static function runUpgradeScripts(Event $event)
    {
        $vendorPath = $event->getComposer()->getConfig()->get('vendor-dir');
        $binPath = $vendorPath . DIRECTORY_SEPARATOR . 'bin' . DIRECTORY_SEPARATOR;
        exec($binPath . 'n98-magerun --skip-root-check sys:setup:run', $output);
    }

    public static function reindexAll(Event $event)
    {
        $vendorPath = $event->getComposer()->getConfig()->get('vendor-dir');
        $binPath = $vendorPath . DIRECTORY_SEPARATOR . 'bin' . DIRECTORY_SEPARATOR;
        exec($binPath . 'n98-magerun --skip-root-check index:reindex:all', $output);
    }

    private static function existsLocalConf($magentoRootDir)
    {
        return (file_exists(self::getConfigurationFilePath($magentoRootDir)));
    }

    private static function overwriteConf()
    {
        return (array_key_exists(self::OVERWRITE_CONF, $_SERVER));
    }

    private static function getConfigurationFilePath($magentoRootDir)
    {
        return $magentoRootDir . '/app/etc/local.xml';
    }
}
