<?php
namespace Bitbull\Deploy;

use Composer\Script\Event;
use Composer\Composer;

class Wordpress
{
    // Overwrite conf flag
    const OVERWRITE_CONF = "OVERWRITE_CONF";
    const FORCE_UPDATE_CONF = 'WP_FORCE_UPDATE';

    private static $_vars = array(
        'WP_DB_NAME',
        'WP_DB_USERNAME',
        'WP_DB_PASSWORD',
        'WP_DB_HOST',
        'WP_AUTH_KEY',
        'WP_SECURE_AUTH_KEY',
        'WP_LOGGED_IN_KEY',
        'WP_NONCE_KEY',
        'WP_AUTH_SALT',
        'WP_SECURE_AUTH_SALT',
        'WP_LOGGED_IN_SALT',
        'WP_NONCE_SALT',
        'WP_DB_TABLE_PREFIX',
        'WP_DEBUG'
    );

    private static $_vars_multisite = array(
        'WP_ALLOW_MULTISITE',
        'WP_MULTISITE',
        'WP_MU_SUBDOMAIN_INSTALL',
        'WP_MU_DOMAIN_CURRENT_SITE',
        'WP_MU_PATH_CURRENT_SITE',
        'WP_MU_SITE_ID_CURRENT_SITE',
        'WP_MU_BLOG_ID_CURRENT_SITE',
    );

    private static $_config = array();

    public static function generateWpConfig(Event $event)
    {
        $vendorPath = $event->getComposer()->getConfig()->get('vendor-dir');
        $extra = $event->getComposer()->getPackage()->getExtra();

        $wordpressRootDir = realpath(dirname($vendorPath) . '/' . $extra['webroot-dir']);

        // Be careful: we are assuming here that vendor dir is always inside the project root dir
        $projectPath = dirname($vendorPath);

        $event->getIO()->write("<info>Generate configuration file for Wordpress</info>");

        if (self::existsLocalConf($wordpressRootDir) && !self::overwriteConf()) {
            $event->getIO()->write("wp-config.php already exists, i will skip it!");
            return;
        }

        if (file_exists($projectPath . '/.env')) {
            $dotenv = new \Dotenv\Dotenv($projectPath);
            $dotenv->load();
        } else if (file_exists($wordpressRootDir . '/.env')) {
            $dotenv = new \Dotenv\Dotenv($wordpressRootDir);
            $dotenv->load();
        }


        $configVars = self::isMultisiteConf() ? array_merge(self::$_vars, self::$_vars_multisite) : self::$_vars;

        foreach ($configVars as $var) {
            if (!array_key_exists($var, $_SERVER)) {
                throw new \RuntimeException("Missing environment variable \"{$var}\"");
            }

            $event->getIO()->write("Env variable key -> \"$var\": \"$_SERVER[$var]\"");

            self::$_config[$var] = $_SERVER[$var];
        }

        // autoupdate wordpress
        if( array_key_exists(self::FORCE_UPDATE_CONF, $_SERVER) && $_SERVER[self::FORCE_UPDATE_CONF] ){
            self::$_config[self::FORCE_UPDATE_CONF] = true;
        }else{
            self::$_config[self::FORCE_UPDATE_CONF] = false;
        }

        $templateFilename = self::isMultisiteConf() ? 'wp-config-multisite.php.twig' : 'wp-config.php.twig';
        $configurationContent = Utils::getConfigurationContent($event->getComposer(), $templateFilename, self::$_config);
        $configurationFilepath = self::getConfigurationFilePath($wordpressRootDir);
        file_put_contents($configurationFilepath, $configurationContent);

        $event->getIO()->write("Saved wp-config.php configuration");
    }

    private static function isMultisiteConf()
    {
        return (array_key_exists('WP_ALLOW_MULTISITE', $_SERVER) && $_SERVER['WP_ALLOW_MULTISITE'] == 'true') ;
    }

    /**
     * @return boolean
     */
    private static function existsLocalConf($wordpressRootDir)
    {
        return (file_exists(self::getConfigurationFilePath($wordpressRootDir)));
    }

    /**
     * @return boolean
    */
    private static function overwriteConf()
    {
        return (array_key_exists(self::OVERWRITE_CONF, $_SERVER));
    }

    /**
     * Get the path to configuration file
     *
     * @param string $wordpressRootDir
     * @return string
    */
    private static function getConfigurationFilePath($wordpressRootDir)
    {
        return $wordpressRootDir . '/wp-config.php';
    }
}
